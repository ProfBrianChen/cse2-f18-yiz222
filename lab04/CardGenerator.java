// Name: Yifan Zhang  Course Information: CSE2 Lab04  Date: 2018-9-20
// This program aims to prompt the user to pick a random card from the deck and then decide
// its card identity

// start of class
public class CardGenerator {
  // start of main method
  public static void main(String[] args) {
    
    // Generate a random number from 1 to 52
    int randomNum = (int)(Math.random() * 52) + 1;
            
    // Use switch statement to determine the exact card
    switch ( randomNum % 13 ){
      case 0:
        System.out.print("You picked the King ");
        break;
      case 1:
        System.out.print("You picked the Ace ");
        break;
      case 2:
        System.out.print("You picked the 2 ");
        break;
      case 3:
        System.out.print("You picked the 3 ");
        break;
      case 4:
        System.out.print("You picked the 4 ");
        break;
      case 5:
        System.out.print("You picked the 5 ");
        break;
      case 6:
        System.out.print("You picked the 6 ");
        break;
      case 7:
        System.out.print("You picked the 7 ");
        break;
      case 8:
        System.out.print("You picked the 8 ");
        break;
      case 9:
        System.out.print("You picked the 9 ");
        break;
      case 10:
        System.out.print("You picked the 10 ");
        break;
      case 11:
        System.out.print("You picked the Jack ");
        break;
      case 12:
        System.out.print("You picked the Queen ");
        break;    
    }
    
           // System.out.print((int)(randomNum/4));


    // Use switch statement to determine its group
    switch ( (int)(randomNum / 13) ){
      case 1:
        System.out.println("of Diamonds.");
        break;
      case 2:
        System.out.println("of Clubs.");
        break;
      case 3:
        System.out.println("of Hearts.");
        break;
      case 4:
        System.out.println("of Spades.");
        break;
          
    }
  } // end of main method
} // end of class