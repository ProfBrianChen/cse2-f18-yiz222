// Name: Yifan Zhang  Course information: CSE2 HW08  Date: 2018-11-12
// This program aims to generate a random hands of selected numbers from a shuffled cards.

import java.util.Scanner;

public class Shuffling {
	public static void main(String[] args) { 
		Scanner scan = new Scanner(System.in); 
		//suits club, heart, spade or diamond 
		String[] suitNames={"C","H","S","D"};    
		String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
		String[] cards = new String[52]; 
		String[] hand = new String[5]; 
		int numCards = 5; 
		int again = 1; 
		int index = 51;
		for (int i=0; i<52; i++){ 
		  cards[i]=rankNames[i%13]+suitNames[i/13]; 
		  System.out.print(cards[i]+" "); 
		} 
		System.out.println();
		printArray(cards); 
		shuffle(cards); 
		printArray(cards); 
		
		while(again == 1){ 
		   hand = getHand(cards,index,numCards); 
		   printArray(hand);
		   index = index - numCards;
		   System.out.println("Enter a 1 if you want another hand drawn"); 
		   again = scan.nextInt();
		   // Create a new deck of cards if numCards is greater than the number of cards remaining
		   if ( index < numCards ) {
			   for (int i=0; i<52; i++){ 
					cards[i]=rankNames[i%13]+suitNames[i/13]; 
					System.out.print(cards[i]+" "); 
				} 
				System.out.println();
				printArray(cards); 
				shuffle(cards); 
				printArray(cards); 
				index = 51; // reset the index to 51
				hand = getHand(cards,index,numCards); 
				printArray(hand);
				index -= numCards;
				System.out.println("Enter a 1 if you want another hand drawn"); 
				again = scan.nextInt(); 
		   }
		} 
		
		
	} // end of main method
	
	// Start of printArray method
	// Used to print an array of elements seperated by a space
	public static void printArray(String[] list) {
		for ( int i = 0; i < list.length; i++ ) {
			System.out.print(list[i] + " ");
		}
		System.out.println("\n");
	} // end of printArray method
	
	// Start of the shuffle method
	// Used to shuffle a deck of 52 cards
	public static void shuffle(String[] list) {
		System.out.println("The shuffled cards are: ");
		for ( int i = 0; i < list.length; i++ ) {
			int index = (int)(Math.random() * list.length); // create a random index from 0 to 52
			String temp = list[0]; // store the number in the first index in the temp variable
			list[0] = list[index]; // set the number at the first index to the number at the index value
			list[index] = temp; // set the number at the index value to the temp variable value
		}
	} // end of shuffle method
	
	// Start of getHand method
	// Used to create an array that contains (numCards) numbers of elements whose index start at the last
	public static String[] getHand(String[] list, int index, int numCards) {
		System.out.println("A hand of cards is: ");
			String[] generateHand = new String[numCards]; // create a new array of String
			
			for (int i = 0; i < numCards; i++ ) {
				generateHand[i] = list[index - i]; // set the string at i to the number at the index that counts from the back
			}
			return generateHand;
	} // end of getHand method
	
} // end of class

