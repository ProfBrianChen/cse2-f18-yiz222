// Name: Yifan Zhang  Course Information: CSE2 HW4  Date: 2018-09-22
// This program aims to imitate the game of Craps in which the user either let the program 
// generate two random integers between 1 and 6 or type in two numbers and then the program
// will generate the slang terminology of the sum of the two numbers
// Use only switch statments

import java.util.Scanner; // import the scanner
// Start of class
public class CrapsSwitch {
  // Start of main method
  public static void main (String[] args) {
    // Create a scanner object
    Scanner input = new Scanner(System.in);
    
    // let the user decide whether to randomly pick or type input
    System.out.println("To start the game of Craps, you need to have two rolls of two six sided dice (inclusive integers from 1 to 6)"); // introduce the game
    System.out.print("Enter 0 if you would like to randomly cast dice, " +
                     "Enter 1 if you would like to state the two dice: "); // let the user choose how to play the game
    int userChoice = input.nextInt(); // Declare and assign the value to the intemediate variable for if statement
    
    // declare and initialize two numbers to zero
    int number1 = 0;
    int number2 = 0;
    
    // Switch statement to continue game using the selected type by user
    switch ( userChoice ) {
      case 0: // if the user choose to randomly generate numbers
        number1 = (int)(Math.random() * 6) + 1; // generate the first random integer from 1-6
        number2 = (int)(Math.random() * 6) + 1; // generate the second random integer from 1-6
        System.out.println("The first roll of dice has " + number1); // Display the first number
        System.out.println("The second roll of dice has " + number2); // Display the second number
        break;
      case 1: // if the user choose to enter two numbers
        System.out.print("Enter two integers from 1 to 6 (inclusive and with space in between): "); // Prompt to enter two numbers
        number1 = input.nextInt(); // assign the first input value from keyboard to number 1
        number2 = input.nextInt(); // assign the second input value from keyboaed to number 2
        break;
    }
    
    // Compute the sum of number1 and number2
    int sum = number1 + number2;
    
    // Switch statement to generate the slang terminology and display result
    switch ( sum ) {
      case 2:
        System.out.println("Snake Eyes"); // print slang terminology if number1 = 1 and number2 = 1
        break;
      case 3:
        System.out.println("Ace Deuce"); // print slang terminology if number1 = 2 and number2 = 1
        break;
      case 4:
        switch ( number1 ) {
          case 1:
          case 3:
            System.out.println("Easy Four"); // print slang terminology if number1 = (1 or 3) and number2 = (3 or 1)
            break;
          case 2:
            System.out.println("Hard Four"); // print slang terminology if number1 = 2 and number2 = 2
            break;
        }
        break;
      case 5:
        System.out.println("Fever Five"); // print slang terminology if (number1 = 4 or 3) and (number2 = 1 or 2)
        break;
      case 6:
        switch ( number1 ){
          case 1:
          case 2:
          case 4:
          case 5:
            System.out.println("Easy Six"); // print slang terminology if (number1 = 5 or 4 or 1 ) and (number2 = 1 or 2 or 5)
            break;
          case 3:
            System.out.println("Hard Six"); // print slang terminology if number1 = 3 and number2 = 3
            break;
        }
        break;
      case 7:
        System.out.println("Seven out"); // print slang terminology if sum = 7
        break;
      case 8:
        switch ( number1 ){
          case 2:
          case 6:
          case 3:
          case 5:
            System.out.println("Easy Eight"); // print slang terminology if (number1 = 6 or 5 or 2 or 3)
            break;
          case 4:
            System.out.println("Hard Eight"); // print slang terminology if number1 = 4 and number2 = 4
            break;
        }
        break;
        
      case 9:
        System.out.println("Nine"); // print slang terminology if sum = 9
        break;
      case 10:
        switch ( number1 ) {
          case 4:
          case 6:
            System.out.println("Easy Ten"); // print slang terminology if number1 = 6 or 4
            break;
          case 5:
            System.out.println("Hard Ten"); // print slang terminology if number1 = 5 and number2 = 5
            break;
            }
        break;
      case 11:
        System.out.println("Yo-leven"); // print slang terminology if sum = 11
        break;
      case 12:
        switch ( number1 ) {
          case 6:
            System.out.println("Boxcars"); // print slang terminology if number1 = 6 and number2 = 6
            break;
          default:
            System.out.println("You do not enter valid number(s), please follow the directions and run the program again."); // deal with invalid number case
            break;
        }        
        break;
      default:  
        System.out.println("You do not enter valid number(s), please follow the directions and run the program again."); // deal with invalid number case
            
        } // end of switch statement
  
    }// End of main method
    
            
    
} // End of class
