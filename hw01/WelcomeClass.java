public class WelcomeClass{
  
  public static void main(String args[]){
    
    System.out.println("    -----------");
    System.out.println("    | WELCOME |");
    System.out.println("    -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-Y--I--Z--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("Hi! My name is Yifan Zhang and I am from Shanghai, China. Some of my hobbies include painting, traveling and writing calligraphy. I am very excited to meet everyone in class!");
    
    
  }
}