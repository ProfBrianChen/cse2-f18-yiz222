// Name: Yifan Zhang  Course Information: CSE2 HW09  Date: 2018-11-24
// This program contains several methods so that it is able to print an array without a specific number
// at an index and also print of an array without containing a chosen number

import java.util.Scanner;

public class RemoveElements {
	public static void main(String [] arg){
		Scanner scan =new Scanner(System.in);
	int num[]=new int[10];
	int newArray1[];
	int newArray2[];
	int index,target;
		String answer="";
		do{
	  	System.out.print("Random input 10 ints [0-9]");
	  	num = randomInput();
	  	String out = "The original array is:";
	  	out += listArray(num);
	  	System.out.println(out);
	 
	  	System.out.print("Enter the index ");
	  	index = scan.nextInt();
	  	newArray1 = delete(num,index);
	  	String out1="The output array is ";
	  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
	  	System.out.println(out1);
	 
	      System.out.print("Enter the target value ");
	  	target = scan.nextInt();
	  	newArray2 = remove(num,target);
	  	String out2="The output array is ";
	  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
	  	System.out.println(out2);
	  	 
	  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
	  	answer=scan.next();
		}while(answer.equals("Y") || answer.equals("y"));
	  }
	 
	  public static String listArray(int num[]){
		String out="{";
		for(int j=0;j<num.length;j++){
	  	if(j>0){
	    	out+=", ";
	  	}
	  	out+=num[j];
		}
		out+="} ";
		return out;
	  }
	  
	  // Random input method
	  // Generates an array of 10 random integers between 0 to 9
	  public static int[] randomInput() {
		  int[] list = new int[10]; // create an array of size 10
		  for ( int i = 0; i < list.length; i++ ) {
			  int value = (int)(Math.random() * 10);
			  list[i] = value;
		  }
		  return list;
	  } // End of random input method
	  
	  // Delete method
	  // Deletes the element at a designated position
	  public static int[] delete(int[] list, int pos) {
		  while ( pos > list.length - 1 || pos < 0 ) {
			  System.out.println("The index is not valid.");
			  System.out.print("Enter an index from 0 to " + (list.length - 1) + ": ");
			  Scanner input = new Scanner(System.in);
			  pos = input.nextInt();
		  }
		  int[] newList = new int[list.length - 1]; // create a new array with one fewer elements
		  int i = 0;
		  while ( i < pos ) {
			  newList[i] = list[i]; // copy the element if index is less than position
			  i++;
		  }
		  for ( i = pos; i < list.length -1; i++ ) {
			  newList[i] = list[i + 1]; // copy the next value of the array starting from i reaches position
		  }
		  return newList;
	  } // End of delete method
	  
	  // Remove method
	  // Removes all elements that are identical to the target
	  public static int[] remove(int[] list, int target) {
		  int newLength = list.length;
		  int index = 0;
		  // find the new length of the array without the target number
		  for ( int i = 0; i < list.length; i++ ) {
			  if ( list[i] == target ) {
				  newLength--; // decrease the length by 1 each time a target number is found
				  
			  }
		  }
		  
		  // Print if the element has been found
		  if ( newLength == list.length ) {
			  System.out.println("Element " + target + " was not found");
		  }
		  else {
			  System.out.println("Element " + target + " has been found");
		  }
		  
		  int[] newList = new int[newLength];// Create a new array of the size without the target number
		  
		  // Assign the elements to the new array
		  for ( int i = 0; i < list.length; i++ ) {
			  if ( list[i] != target && i < list.length) {
				  // if the ith element of list is not equal to target,
				  // assign the element of list[i] to the new array at index variable
				  newList[index] = list[i]; 
				  index++; // increment the index of the new array by 1 to store new elements 
			  }
			  
		  }
		  return newList;
	   } // End of remove method
	  
} // End of class