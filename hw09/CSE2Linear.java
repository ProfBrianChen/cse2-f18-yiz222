// Name: Yifan Zhang  Course Information: CSE2 HW09  Date: 2018 - 11 - 24
// This program accepts 15 ascending grades and is able to perform binary and linear search to find elements.

import java.util.Scanner; // import the scanner class
import java.util.Random; // import the random class

public class CSE2Linear {
	public static void main(String[] args) {
		// Create a Scanner object
		Scanner input = new Scanner(System.in);
		
		// Our intermediate and output variables
		int value = 0;
		int[] grades = new int[15];
		
		// Prompt for the input
		System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
		
		// The input validation loop
		for (int i = 0; i < grades.length; i++) {
			System.out.print("Enter the " + (i + 1) + " grade: ");
			// Check if the input value is an integer
			while (!input.hasNextInt()) {
				input.next();
				System.out.print("Enter a grade in integer type: ");
			}
			value = input.nextInt();
			
			// Check if the input value is in the range of 0 - 100
			while (value < 0 || value > 100) {
				System.out.print("Enter an integer in the range of 0 - 100: ");
				
				// First check if the input value is an integer
				while (!input.hasNextInt()) {
					input.next();
					System.out.print("Enter a grade in integer type: ");
				}
				value = input.nextInt(); // Then update the value
				
			}
			
			// Check if the input value is in an ascending order
			while ( i >= 1 && grades[i - 1] > value ) {
				System.out.print("Enter a new integer greater than the last one: ");
				
				// First check if the input value is an integer
				while (!input.hasNextInt()) {
					input.next();
					System.out.print("Enter a grade in integer type: ");
				}
				value = input.nextInt(); // Then assign the value
				
				// Also check for the range
				while (value < 0 || value > 100) {
					System.out.print("Enter an integer in the range of 0 - 100: ");
					value = input.nextInt();
				}
			}
			
			grades[i] = value;
			//System.out.println("grades[" + i + "] is " + grades[i]);	
			
		} // End of the 15 inputs for loop
		
		// Print the Final input array
		System.out.println("The " + grades.length + " final grades are:");
		printArray(grades);
		
		// Prompt the user to enter a grade to be searched for
		System.out.println();
		System.out.print("Enter a grade to search for: ");
		int key = input.nextInt();
		
		// Binary search to find the target grade
		binarySearch(grades, key);
		
		// Scramble array and print the scrambled array
		int[] scrambled = scrambleArray(grades);
		System.out.println("Scrambled:");
		printArray(scrambled);
		
		// Prompt the user again to enter a grade to be searched for
		System.out.println();
		System.out.print("Enter a grade to search for: ");
		key = input.nextInt();
		
		// Linear Search to find the target grade
		linearSearch(scrambled, key);
		
		
		
	} // End of main method
	
	// Start of Binary Search method
	public static void binarySearch(int[] list, int key) {
		int high = list.length - 1, low = 0;
		int iteration = 0;
		boolean keyFound = false;
		while ( high >= low ) {
			int mid = (high + low) / 2;
			// decrease the higher bound by 1 from the midpoint
			// if the key is in the lower half
			if ( key < list[mid] ) {
				high = mid - 1;
				iteration++; // increment the number of iterations
			}
			
			else if ( key == list[mid] ) {
				iteration++; // increment the number of iterations
				System.out.println(key + " is found in the list with " + iteration + " iterations.");
				keyFound = true; // mark that the key is found
				break;
			}
			
			// increase the lower bound by 1 from the midpoint
			// if the key is in the upper half
			else {
				low = mid + 1;
				iteration++; // increment the number of iterations
			}
		}
		
		// Print if the key is not found
		if (keyFound == false) {
			System.out.println (key + " is not found in the list with " + iteration + " iterations.");
		}
	} // End of binary search method
	
	// Start of Scramble Array method
	public static int[] scrambleArray(int[] list) {
		int index, temp;
		Random random = new Random();
		
		for ( int i = list.length - 1; i > 0; i-- ) {
			index = random.nextInt(i + 1); // generate a random index
			temp = list[index];
			list[index] = list[i];
			list[i] = temp; // swap the elements
		}
    
		return list;
	} // End of scramble array method
	
	// Start of Print Array method
	public static void printArray(int[] list) {
		for ( int i = 0; i < list.length; i++ ) {
			System.out.print(list[i] + " ");
		}
	} // End of print array method
	
	// Start of Linear Search
	public static void linearSearch(int[] list, int key) {
		int iteration = 0;
		boolean keyFound = false; // initialize the key found to false
		// The for loop that searches for the entire list one by one
		for ( int i = 0; i < list.length; i++ ) {
			if ( key == list[i] ) {
				iteration++;
				System.out.println(key + " is found in the list with " + iteration + " iterations.");
				keyFound = true;
				break; // break out of the loop if the key is found
			}
			
			else {
				iteration++;
			}
		} // end of the for loop that goes through the entire list
		
		if (keyFound == false) {
			System.out.println (key + " is not found in the list with " + iteration + " iterations.");
		}
	} // End of linear search method
	
	
} // End of class
