// Name: Yifan Zhang  Course information: CSE2 HW3  Date: 2018-09-16
// This program aims to compute the volume inside the pyramid 
// by inputing the length of each side of the pyramid and the height

import java.util.Scanner; // import the Scanner to scan inputs from keyboard
// start of class
public class Pyramid {
  // start of main method
  public static void main(String[] args) {
    // create a scanner
    Scanner input = new Scanner(System.in);
    
    // Prompt to enter input variables, declare and assign values
    System.out.print("The square side of the pyramid is (input length): "); // Prompt to enter the side length of the pyramid
    double length = input.nextDouble(); // declare and assign the input value to variable length
    System.out.print("The height of the pyramid is (input height): "); // Prompt to enter the height of the pyramid
    double height = input.nextDouble(); // declare and assign the input value to variable height
    
    // Compute the volume inside the pyramid
    // formula: Volume = 1 / 3 * (side length)^2 * height
    double volume = (length * length * height) / 3;
    
    // Display the result of volume
    System.out.println("The volume inside the pyramid is: " + volume);//  * 100) / 100.0);
    
  } // end of main method
} // end of class