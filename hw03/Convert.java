// Name: Yifan Zhang  Course information: CSE2 HW3  Date: 2018-09-16
// This program aims to compute the quantity of rain in cubic miles
// by inouting the number of acres of land affected and the inches of rain dropped

import java.util.Scanner; // import the scanner to input variable values from keyboard
// start of class
public class Convert {
  // start of main method
  public static void main(String[] args) {
    // create a scanner
    Scanner input = new Scanner(System.in);
    
    // Prompt to enter input variables, declare and assign values
    System.out.print("Enter the affected area in acres: "); // prompt to enter area in acres
    double acres = input.nextDouble(); // declare and assign the value to variable acres
    System.out.print("Enter the rainfall in the affected area: "); // prompt to enter the height of rain in inches
    double inches = input.nextDouble(); // declare and assign value to variable inches
    
    // Declare and assign value to intermediate variables
    double inchesPerFoot = 12; // the value of conversion between inches and feet
    double feetPerMile = 5280; // the value of conversion between feet and miles
    double milesPerAcre = 1 / 640.0; // the value of conversion between miles and acres
    
    // Compute the area of rain in square miles
    double area = acres * milesPerAcre; // convert the area in acres into square miles
    
    // Compute the height of rain in miles
    double height = inches / inchesPerFoot / feetPerMile; // convert the height from inches to feet and then to miles
    
    // Compute the volume of rain in cubic miles
    double volume = area * height;
    
    // Display the result of volume of rain
    System.out.println("The quantity of rain is " + volume + " cubic miles.");
    
  } // end of main method
} // end of class