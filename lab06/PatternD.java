// Name: Yifan Zhang  Course Information: CSE2 Lab06  Date: 2018-10-11
// This program aims to generate a pattern using nested loops

import java.util.Scanner; // import the scanner
// Start of class
public class PatternD {
	// Start of main method
	public static void main(String[] args) {
		// Create a scanner object
		Scanner input = new Scanner(System.in);
			
		// Prompt to input a number
		System.out.print("Input an integer between 1 and 10 as the length of the pyramid: ");
		int integer = 0;
			
		boolean condition = false; // set the initial condition to false
		while( condition == false ) {
		   if( input.hasNextInt() == true ) {
			   integer = input.nextInt(); // read the input and store its value if it is an integer
			   if( integer < 1 || integer > 10 ) {
				   condition = false; // let the while loop run again if integer is out of range
				   System.out.println("Need to input an integer between 1 and 10 inclusive: ");
				   }
			   else {
				   condition = true; // get out of the inner statement and while loop if condition is true
				   }    
		       } // end of the inner if statement
		   
		   else {
			   input.next(); // get rid of the value stored in the input scanner
			   System.out.println("Need to input an integer between 1 and 10 inclusive: ");
			   } // end of the outer if statement
		   
		   }// end of while loop
		
			
		// Nested loops
		// The outer loop is responsible for the number of rows for the pattern
		for(int numRows = integer; numRows >= 1; numRows--) {
			// The inner loop is responsible for displaying the numbers on each row
			for (int i = numRows; i >= 1 ; i--) {
				System.out.print(i + " "); // print the number i
			}				
			
			System.out.println(); // print a new line every time after finishing the inner loop	
			
		} // End of for loop
			
	} // End of main method


}
