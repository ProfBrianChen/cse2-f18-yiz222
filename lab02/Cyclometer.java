// Yifan Zhang, 9-6-2018, CSE2 lab 02
//
public class Cyclometer {
  // start of main method
  public static void main(String[] args) {
    // input data.
    int secsTrip1=480;  // number of seconds for the first trip to compute the number of minutes for the first trip
    int secsTrip2=3220;  // number of seconds for the second trip to compute the number of minutes for the second trip
    int countsTrip1=1561;  // number of counts for the first trip to print and compute distance traveled
    int countsTrip2=9037; // number of counts for the second trip to print and compute distacne traveled
    
    // intermediate variables and output data
    double wheelDiameter=27.0;  // the diameter of the bike's wheel to compute distance traveled
  	double PI=3.14159; // the constant value of PI used when calculating the distance traveled
  	double feetPerMile=5280;  // the value for conversion between feet and mile
  	double inchesPerFoot=12;   // the value for conversion between inches and feet
  	double secondsPerMinute=60;  // the value for conversion between seconds and minute
	  double distanceTrip1, distanceTrip2, totalDistance;  // declare three output variables, which are distance traveled for trip 1, trip 2 and total distance
    
    // print the time in minutes for each trip and number of counts for each trip
    System.out.println("Trip 1 took "+
       	     (secsTrip1 / secondsPerMinute) + " minutes and had " +
       	      countsTrip1 + " counts."); // time in minutes for trip 1 and number of counts for trip 1
	       System.out.println("Trip 2 took " +
       	     (secsTrip2 / secondsPerMinute) + " minutes and had " +
       	      countsTrip2 + " counts."); // time in minutes for trip 2 and number of counts for trip 2
    
    //run the calculations; store the values. Document your
    // first, compute distance for each trip = number of counts * diameter of the wheel * PI 
    // this gives the distance for each trip in inches 
    // second, convert the distance in inches into feet and then to miles
    // this gives the distance for each trip in miles
    // third, add the distance for each trip to compute the total distance
    distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip1 /= inchesPerFoot * feetPerMile; // Gives distance in miles for trip 1
	  distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; // gives distance in miles for trip 2
	  totalDistance = distanceTrip1 + distanceTrip2; // gives total distance for two trips in miles
    
    //Print out the output data. 
    System.out.println("Trip 1 was "+distanceTrip1+" miles"); // print the distance for trip 1
	  System.out.println("Trip 2 was "+distanceTrip2+" miles"); // print the distance for trip 2
	  System.out.println("The total distance was "+totalDistance+" miles"); // print the total distance

    
  } // end of main method 
} // end of class