//Name: Yifan Zhang  Course Information: CSE2 Lab09  Date: 2018-11-15
// This program aims to print some arrays by passing through certain methods

public class Lab09 {
	public static void main(String[] agrs) {
		int[] array0 = { 3, 4, 5, 2, 6, 19, 49, 30, 5};
		int[] array1 = copy(array0);
		int[] array2 = copy(array0);
		
		print(inverter(array0));
		print(inverter2(array1));
		int[] array3 = inverter(array2);
		print(array3);
	}
	
	public static int[] copy(int[] num) {
		int[] newArray = new int[num.length];
		for ( int i = 0; i < newArray.length; i++ ) {
			newArray[i] = num[i];
		}
		return newArray;
	}
	
	public static int[] inverter(int[] num) {
		int[] invertArray = new int[num.length];
		for ( int i = 0; i < invertArray.length; i++ ) {
			invertArray[i] = num[invertArray.length - 1- i];
		}
		return invertArray;
		
	}
	
	public static int[] inverter2(int[] inputArray) {
		int[] copyArray = copy(inputArray);
		int[] invertArray = new int[copyArray.length];
		for ( int i = 0; i < invertArray.length - 1; i++ ) {
			invertArray[i] = copyArray[invertArray.length - 1- i];
		}
		return copyArray;
		
	}
	
	public static void print(int[] num) {
		for ( int i = 0; i < num.length; i++ ) {
			System.out.print(num[i] + " ");
		}
		System.out.println();
	}
	
	
}
