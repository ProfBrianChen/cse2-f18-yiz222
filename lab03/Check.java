// Name: Yifan Zhang   Class information: CSE 2 lab 03   Date: 2018-09-13
// This program aims to the cost that each person is supposed to pay by 
// inputing the original cost of the dinner, the percentage tip that each person is willing to pay, the number of people

import java.util.Scanner;
// Start of class
public class Check {
  // start of main method
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in); // create a scanner
    System.out.print("Enter the original cost of the check in the form xx.xx: "); // Prompt to enter the orginal cost
    double checkCost = input.nextDouble(); // declare a double variable of checkCost and assign the typed-in value
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = input.nextDouble(); // declare a double variable of tip percent and assign the typed-in value
    tipPercent /= 100; // convert the percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = input.nextInt(); // declare a double variable of the number of people and assign the typed-in value
    double totalCost; // declare a double variable of total cost 
    double costPerPerson; // declare a double variable of cost per person
    int dollars,   //whole dollar amount of cost 
      dimes, pennies; //for storing digits
          //to the right of the decimal point for the cost$ 
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople; //get the whole amount, dropping decimal fraction
    dollars = (int)costPerPerson;
    //get dimes amount, e.g., 
    // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
    //  where the % (mod) operator returns the remainder
    //  after the division:   583%100 -> 83, 27%5 -> 2 
    dimes=(int)(costPerPerson * 10) % 10; // calculate the dimes
    pennies=(int)(costPerPerson * 100) % 10; // calculate the pennies
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
    
  } // end of main method
} // end of class
