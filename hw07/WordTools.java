// Name: Yifan Zhang  Course information: CSE2 HW07  Date: 2018-10-29
// This program aims to construct several word tools for a input string.

import java.util.Scanner; // import the scanner 
// Start of class
public class WordTools {
	// sampleText() method
	// prompts the user to enter a string of their choosing
	public static String sampleText() {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter a sample text: ");
		String sampleText = input.nextLine();
		return sampleText;
		
	} // end of sample Text method
	
	
	// printMenu() method
	// outputs a menu of user options for analyzing/editing the string
	// and returns the user's entered menu option
	public static String printMenu() {
		String menu = ("MENU \n"
				+ "c - Number of non-whitespace characters \n"
				+ "w - Number of words \n"
				+ "f - Find text \n"
				+ "r - Replace all !'s \n"
				+ "s - Shorten spaces \n"
				+ "q - Quit \n");
		
		return menu;
	} // end of printMenu method
	
	// getNumOfNonWSCharacters() method
	// returns the number of characters in the String, exclude whitespace
	public static String getNumOfNonWSCharacters(String text) {
		int characters = text.length();
		for ( int i = 0; i <= text.length() - 1; i++ ) {
			if ( text.charAt(i) == ' ' ) {
				characters--;
			} // decrease the length by 1 if it is a whitespace
		} // end of for loop to repeat running through the entire string of each char
		
		return ("Number of non-whitespace characters: "+ characters);
	} // end of getNumOfNonWSCharacters method
	
	
	// getNumOfWords() method
	// returns the number of words in the string
	public static String getNumOfWords(String text) {
		int words = 1;
		for ( int i = 0; i <= text.length() - 1; i++ ) {
			if ( text.charAt(i) == ' ' ) {
				words++;
			} // increment the word count by 1 if there is a space
		} // end of for loop to repeat running through each char in the string
		
		return ("Number of words: " + words);
	} // end of getNumOfWords() method
	
	
	// findText() method
	// returns the number of instances a word or phrase is found in the string
	public static String findText(String t1, String t2) {
		int count = 0; // initialize count to 0
		int find = t2.indexOf(t1); // initialize find to an index number
		// find should be a positive number if t1 is a word in the string of t2
		// find should be -1 if there is no t1 found in t2
		
		// repeat finding other t1 words in the string t2 if there is one at least present
		while ( find >= 0 ) {
			find++; // start from the index after the first found
			count++; // increment the count by 1
			
			find = t2.indexOf(t1, find); // the next index where t1 is found in t2
			
		}
		return ("\"" + t1 + "\" instances: " + count); // return the number of occurrence of t1
	} // end of findText() method
	
	
	// replaceExclamation() method
	// returns a string which replaces each '!' character in the string with a '.'
	public static String replaceExclamation(String text) {
		String exclamation = text.replace("!", ".");
		return exclamation;
	} // end of replaceExclamation() method
	
	
	// shortenSpace() method
	// returns a string that replaces all sequences of 2 or more spaces with a single space.
	public static String shortenSpace(String text) {
		String shortenSpace = text.replace("  ", " ");
		return shortenSpace;
	} // end of shortenSpace() method
	
	
	// Start of main method
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String userText = sampleText();
		System.out.println("\nYou entered: " + userText + "\n");
		System.out.println(printMenu());
		System.out.println("Choose an option: ");
		
		String enter = input.nextLine();
		int flag = 0;
		while ( true ) {
			
			switch(enter) {
				case "c":
					System.out.println(getNumOfNonWSCharacters(userText) + "\n");
					break;
				case "w":
					System.out.println(getNumOfWords(shortenSpace(userText)) + "\n");
					break;
				case "f":
					System.out.print("Enter a word or phrase to be found: ");
					String userWord = input.nextLine();
					System.out.println(findText(userWord, userText) + "\n");
					break;
				case "r":
					System.out.println(replaceExclamation(userText) + "\n");
					break;
				case "s":
					System.out.println(shortenSpace(userText) + "\n");
					break;
				case "q":
					System.exit(0); // quit the program
					break;
				default:
					//input.nextLine();
					System.out.println(printMenu() + "\nYou need to input according to the options."
							+ "\nChoose an option: ");
					enter = input.nextLine();
					flag = 1; // make this default case the only case with flag equals 1.
					
			
				
			} // end of switch statement
			
			System.out.println(printMenu()); // print menu
			System.out.println("Choose an option: "); // print choose option
			if (flag != 1) {
			enter = input.nextLine(); // use the new input if there is a valid input
			}
			flag =0;
		
	} // end of while loop
		
	}// End of main method

} // end of class
