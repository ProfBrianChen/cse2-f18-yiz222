//Name: Yifan Zhang  Course Information: CSE2 lab05  Date: 2018-10-04
// This program aims to ask the course information and check if inputs are right 
import java.util.Scanner; // import scanner
// Start of class
public class ErrorInputs {
  public static void main(String[] args) {
	  	
	  	// Course number (integer)
	    int Course_Num = 0;
	    Scanner courseNum = new Scanner(System.in);
	    System.out.print("Enter the course number: ");
	    while ( courseNum.hasNextInt() != true ) {
	      System.out.println("You need input of integer type.");
	      System.out.print("Enter the course number as an integer: ");
	      courseNum.next();
	    }
	    Course_Num = courseNum.nextInt();
	    System.out.println(Course_Num);
	    
	    // Department name (string)
	    String Department_Name = null;
	    Scanner departmentName = new Scanner(System.in);
	    System.out.print("Enter the department name as a string: "); // Prompt for department name input
	    while ( departmentName.hasNext() == false ) {
	      System.out.println("You need input of string type.");
	      System.out.print("Enter the department name as a string: "); // Prompt for course number input
	      departmentName.nextLine();// obtain the department name
	    }
	    Department_Name = departmentName.nextLine(); // Obtain the department name
	    System.out.println(Department_Name);
	    
	    
	    //the number of times it meets in a week(integer)
	    int time_Num = 0;
	    Scanner time = new Scanner(System.in);
	    System.out.print("Enter the number of times it meets as an integer:");
	    while ( time.hasNextInt() == false ) {
	      System.out.println("You need input of integer type.");
	      System.out.print("Enter the meeting time as an integer: ");
	      time.next();
	    }
	    time_Num = time.nextInt();
	    System.out.println(time_Num);
	    
	    // The time that the class starts (integer)
	    int time_Starts = 0;
	    Scanner timeStarts = new Scanner(System.in);
	    System.out.print("Enter the time the class starts as an integer:");
	    while ( timeStarts.hasNextInt() == false ) {
	      System.out.println("You need input of integer type.");
	      System.out.print("Enter the time starts as an integer: ");
	      timeStarts.next();
	    }
	    time_Starts = timeStarts.nextInt();
	    System.out.println(time_Starts);
	    
	    // The instructor name (string)
	    String Instructor_Name = null;
	    Scanner instructorName = new Scanner(System.in);
	    System.out.print("Enter the instructor name as a string: "); // Prompt for department name input
	    while ( instructorName.hasNext() == false ) {
	      System.out.println("You need input of string type.");
	      System.out.print("Enter the instructor name as a string: "); // Prompt for course number input
	      instructorName.nextLine();// obtain the department name
	    }
	    Instructor_Name = instructorName.nextLine(); // Obtain the department name
	    System.out.println(Instructor_Name);
	    
	    // Number of students (integer)
	    int Student_Num = 0;
	    Scanner studentNum= new Scanner(System.in);
	    System.out.print("Enter the number of students as an integer:");
	    while ( studentNum.hasNextInt() == false ) {
	      System.out.println("You need input of integer type.");
	      System.out.print("Enter the student number as an integer: ");
	      studentNum.next();
	    }
	    Student_Num = studentNum.nextInt();
	    System.out.println(Student_Num);
	    

	  } // End of main method
	} // End of class

