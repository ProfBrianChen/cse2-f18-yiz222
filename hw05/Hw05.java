// Name: Yifan Zhang  Course Information: CSE2 HW5  Date: 2018-10-07
// The purpose of this program is to calculate the probability of different hands occurring if generating certain number of hands.
// This program uses while loops to achieve this goal

// import the scanner
import java.util.Scanner;

// Start of class
public class Hw05 {
	// Start of main method
	public static void main(String[] args) {
		// Create a scanner object
		Scanner input = new Scanner(System.in);
		
		// Ask for how many hands to generate
		System.out.print("The number of loops(as an integer): ");
		int loops = input.nextInt(); // Use the scanner to obtain the number of hands
		
		// Initiate intermediate variables
		int fourOfAKind = 0, threeOfAKind = 0, twoPair = 0, onePair = 0;
		
		// While loops for all hands
		int i = 0; // initiate i to zero
		while ( i < loops ) {
      
			// Generate five random numbers from 1 to 52 for each hand
			int num1 = (int)(Math.random()*52 + 1);
			int num2 = (int)(Math.random()*52 + 1);
			int num3 = (int)(Math.random()*52 + 1);
			int num4 = (int)(Math.random()*52 + 1);
			int num5 = (int)(Math.random()*52 + 1);
			
			// Check if these five numbers are same
			// If same, generate a new number
			if ( num2 == num1 ) {
				num2 = (int)(Math.random()*52 + 1);
			}
			else if ( num3 == num1 || num3 == num2 ) {
				num3 = (int)(Math.random()*52 + 1);
			}			
			else if ( num4 == num1 || num4 == num2 || num4 == num3 ) {
				num4 = (int)(Math.random()*52 + 1);
			}
			else if ( num5 == num1 || num5 == num2 || num5 == num3 || num5 == num4 ){
				num5 = (int)(Math.random()*52 + 1);
			}
			
			// Divide each number by 13, obtain the remainder
			// This  is the identity of card (the number displayed on card) 
			// regardless of the card class
			int rem1 = num1 % 13;
			int rem2 = num2 % 13;
			int rem3 = num3 % 13;
			int rem4 = num4 % 13;
			int rem5 = num5 % 13;
			
			// Check if the set of five hands belong to one of the four special hands
			// If so, increment the counter for that hand by 1
			// Check for Four-of-a-kind cases

			if ( rem1 == rem2 && rem1 == rem3 && rem1 == rem4 
					|| rem1 == rem2 && rem1 == rem3 && rem1 == rem5
					|| rem1 == rem2 && rem1 == rem4 && rem1 == rem5
					|| rem1 == rem3 && rem1 == rem4 && rem1 == rem5
					|| rem2 == rem3 && rem2 == rem4 && rem2 == rem5 ) {
				fourOfAKind += 1;
			}
			
			// Check for Three-of-a-kind cases
			else if (rem1 == rem2 && rem1 == rem3
					|| rem1 == rem2 && rem1 == rem4
					|| rem1 == rem2 && rem1 == rem5
					|| rem1 == rem3 && rem1 == rem4
					|| rem1 == rem3 && rem1 == rem5
					|| rem1 == rem4 && rem1 == rem5
					|| rem2 == rem3 && rem2 == rem4
					|| rem2 == rem3 && rem2 == rem5
					|| rem2 == rem4 && rem2 == rem5
					|| rem3 == rem4 && rem3 == rem5 ) {
					threeOfAKind += 1;
			}
			
			// Check for Two-pair cases
			if ( rem1 == rem2 && ( rem3 == rem4 || rem3 == rem5 || rem4 == rem5 ) 
					|| ( rem1 == rem3 && ( rem2 == rem4 || rem4 == rem5 || rem2 == rem5 ) )
					|| ( rem1 == rem4 && ( rem2 == rem3 || rem3 == rem5 || rem2 == rem5 ) ) 
					|| ( rem1 == rem5 && ( rem2 == rem3 || rem3 == rem4 || rem2 == rem4 ) ) 
					|| ( rem2 == rem3 && ( rem4 == rem5 ) )
					|| ( rem2 == rem4 && ( rem3 == rem5 ) ) 
					|| ( rem2 == rem5 && ( rem3 == rem4 ) ) ) {
					twoPair += 1;		
			}
			
			// Check for One-pair cases
			else if ( rem1 == rem2 || rem1 == rem3 || rem1 == rem4 || rem1 == rem5 
					|| rem2 == rem3 || rem2 == rem4 || rem2 == rem5
					|| rem3 == rem4 || rem3 == rem5 || rem4 == rem5 ) {
					onePair += 1;				
			}
				
			i++; // increment the while loop by 1
		} // End of while loop
		
		
		// Calculate the probabilities for each of the four hands
		// Formula: probability = number of occurrences / total number of hands
		double P_FourOfAKind = (fourOfAKind * 100) / 100.0 / loops ;
		double P_ThreeOfAKind = (threeOfAKind * 100) / 100.0 / loops;
		double P_TwoPair = (twoPair * 100) / 100.0 / loops;
		double P_OnePair = (onePair * 100) / 100.0 / loops;
		
		// Display result
		System.out.printf("The probability of Four-of-a-kind: %.3f \n", P_FourOfAKind);
		System.out.printf("The probability of Three-of-a-kind: %.3f \n", P_ThreeOfAKind);
		System.out.printf("The probability of Two-pair: %.3f \n", P_TwoPair);
		System.out.printf("The probability of One-pair: %.3f \n", P_OnePair);
		
		
	} // End of main method
	
} // End of class