// Name: Yifan Zhang  Course information: CSE lab08  Date: 2018-11-8
// This program creates a first array with random numbers from 0 to 99 and the second array
// counts the number of occurrence of each random numbers

public class Lab08 {
	public static void main(String[] args) {
		// Create two arrays of size 100
		int[] myArray1 = new int[100];
		int[] myArray2 = new int[100];
		int temp = 0;
		//int count = 0;
		
		// Fill in the first array with random numbers from 0 to 99
		for (int i = 0; i < myArray1.length; i++ ) {
			myArray1[i] = (int)(Math.random() * 100);
					
		}
		
		// Fill in the second array with the number of occurrence of the first array
		for ( int j = 0; j < myArray2.length; j++ ) {
			temp = myArray1[j]; // store the current number in the temp variable
			myArray2[temp]++; // increment the count the number of occurrence by 1 for the temp item in the array
		}
		
		// Print the occurence of numbers
		for ( int k = 0; k < myArray2.length; k++ ) {
			if(myArray2[k] > 0 && myArray2[k] == 1){
				System.out.printf("%d occurs %d time\n",k, myArray2[k]); // print the occurrence of 1 if the number occurs once
				//count+= myArray2[k];
				}
	        else if(myArray2[k] >=2){
	            System.out.printf("%d occurs %d times\n",k, myArray2[k]); // print the occurrence of k if the number occurs greater than 1
	            //count+= myArray2[k];
	            }
		}
		
		//System.out.println("Count is " + count);
	} // end of main method

} // end of class