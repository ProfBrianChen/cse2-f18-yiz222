// Name: Yifan Zhang   Course information: CSE 2 HW2   Date: 2018-09-08
// The purpose of this program is to compute the total cost of the items bought at a store, including the sales tax.
// This goal can be achieved by making calculations among the variables which have assigned value and then printing out the result.

public class Arithmetic{
  // start of main method
  public static void main(String[] args) {
    // input data    
    int numPants = 3;// Number of pairs of pants   
    double pantsPrice = 34.98;// Cost per pair of pants

    int numShirts = 2;// Number of sweatshirts
    double shirtPrice = 24.99;// Cost per shirt

    int numBelts = 1;// Number of belts
    double beltCost = 33.99;// cost per belt

    double paSalesTax = 0.06;// the tax rate
    
    // intermediate variables and output data
    // Total cost of each item without tax
    // This is equal to the number of that item multiply by the item price
    double totalCostOfPants = numPants * pantsPrice; // Total cost of pants
    double totalCostOfShirts = numShirts * shirtPrice; // Total cost of sweatshirts
    double totalCostOfBelts = numBelts * beltCost; // Total cost of belts
    System.out.println("The total cost of each item without sales tax:");// Start of printing cost of each item without tax
    System.out.println("  The total cost of pants without sales tax is $" + totalCostOfPants + ".");//Print total cost of pants without tax
    System.out.println("  The total cost of sweatshirts without sales tax is $" + totalCostOfShirts + ".");//Print total cost of shirts without tax
    System.out.println("  The total cost of belts without sales tax is $" + totalCostOfBelts + ".");//Print total cost of belts without tax
    
    // Sales tax charged buying all each kind of item
    // This is equal to the total cost of that item multiply by the PA sales tax rate
    double taxOfPants = totalCostOfPants * paSalesTax; // Sales tax on pants
    double taxOfShirts = totalCostOfShirts * paSalesTax; // Sales tax on sweatshirts
    double taxOfBelts = totalCostOfBelts * paSalesTax; // Sales tax on belts
    System.out.println("The sales tax on each item:");//Start of printing sales tax on each item
    System.out.println("  The sales tax on pants is $" + (int)(taxOfPants * 100) / 100.0 + "." ); // Print sales tax on pants
    System.out.println("  The sales tax on sweatshirts is $" + (int)(taxOfShirts * 100) / 100.0 + ".");// Print sales tax on sweatshirts
    System.out.println("  The sales tax on belts is $" + (int)(taxOfBelts * 100) / 100.0 + ".");// Print sales tax on belts
    
    // Total cost of purchases without sales tax
    // This is equal to the sum of total cost of each item
    double totalCost = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    System.out.println("The total cost of the purchases without sales tax is $" + totalCost + "."); // Print the total cost without sales tax
    
    // Total sales tax
    // This is equal to the sum of sales tax on each item
    double totalTax = taxOfPants + taxOfShirts + taxOfBelts;
    System.out.println("The total sales tax of all items is $" + (int)(totalTax * 100) / 100.0 + "."); // Print the total sales tax
    
    // Total cost of purchases with sales tax
    // This is equal to the sum of total cost of purchases without sales tax and the total sales tax
    double totalCostAndTax = totalCost + totalTax;
    System.out.println("The total cost of the purchases including sales tax is $" + (int)(totalCostAndTax * 100) / 100.0 + ".");

    
  } // end of main method
} // end of class