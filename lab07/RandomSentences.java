// Name: Yifan Zhang  Course information: CSE2 Lab07
// This program generates random words to make a sentence.

import java.util.Random; // import the Random
import java.util.Scanner;
// Start of class
public class RandomSentences {
	// Start of main method
	public static void main(String[] args) {
		// Create a Scanner
		Scanner input = new Scanner(System.in);
		// Prompt for input
		System.out.print("Do you like to generate a random sentence? (Enter any number to generate, 0 to quit):");
		int choice = input.nextInt();
		int phase = 0;
		// First Phase while loop
		while ( choice != 0  ) {
			// Create a random object
			Random randomGenerator = new Random();
			// Generate random integers from 0 to 9
			int randomInt = randomGenerator.nextInt(10);
			int randomInt1 = randomGenerator.nextInt(10);
			while (randomInt1 == randomInt) {
				randomInt1 = randomGenerator.nextInt(10);
			}
			int randomInt2 = randomGenerator.nextInt(10);
			while (randomInt2 == randomInt || randomInt2 == randomInt1) {
				randomInt2 = randomGenerator.nextInt(10);
			}
			// Print out the first phase sentence
			System.out.println("The " + adj(randomInt) + " "+ adj(randomInt1) 
			+ " "+ subject(randomInt) + " " + verb(randomInt) + " the " + adj(randomInt2) + " "
			+ object(randomInt) + ".");
			
			// Prompt for input after the first time 
			System.out.print("Do you like to generate a random sentence? \n\t Enter any number to generate a new sentence "
					+ "\n\tEnter 2 to go to phase 2 \n\t Enter 0 to quit \nPlease enter: ");
			
			
			// Change to phase 2 if the user input 2
			choice = input.nextInt();
			if ( choice == 2 ) {
			phase = 2;
			System.out.print(phase2First(randomInt) + phase2Second(randomInt) + phase2Final(randomInt));
			
			}
			//9input.next();
			} // end of outer while loop
		} // end of main method
		
	// Method for Second Phase Thesis Sentence
	public static String phase2First(int num) {
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(10);
		int randomInt1 = randomGenerator.nextInt(10);
		while (randomInt1 == randomInt) {
			randomInt1 = randomGenerator.nextInt(10);
		}
		int randomInt2 = randomGenerator.nextInt(10);
		while (randomInt2 == randomInt || randomInt2 == randomInt1) {
			randomInt2 = randomGenerator.nextInt(10);
		}
		
		String result = ("The " + adj(randomInt) + " "+ adj(randomInt1) 
		+ " "+ subject(num) + " " + verb(randomInt) + " the " + adj(randomInt2) + " "
		+ object(randomInt) + ".\n");
		return result;
	}
	
	// Method for Second Phase Action Sentences
	public static String phase2Second(int num) {
		String result = "";
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(10);
		int randomInt1 = 0, randomInt2 = 0,randomInt3 = 0;
		for ( int n = 0; n < randomInt; n++ ) {
			int randomN = randomGenerator.nextInt(2);
			randomInt1 = randomGenerator.nextInt(10);
			while (randomInt1 == randomInt) {
			randomInt1 = randomGenerator.nextInt(10);
			}
			randomInt2 = randomGenerator.nextInt(10);
			while (randomInt2 == randomInt || randomInt2 == randomInt1) {
				randomInt2 = randomGenerator.nextInt(10);
			}
			randomInt3 = randomGenerator.nextInt(10);
			while ( randomInt3 == randomInt || randomInt3 == randomInt1 || randomInt3 == randomInt2 ) {
				randomInt3 = randomGenerator.nextInt(10);
			}
			switch (randomN) {
				case 0:
					result += "It";
					break;
				case 1:
					result += "That " + subject(num);
					break;
				} // end of switch statement
			result += (" "+ verb(randomInt1) + " the " + adj(randomInt1) + " "
					+ object(randomInt1) + ".\n");	
		} // end of for loop
		result += "";
		return result;
	} // end of the phase2Second method
	
	// Method for Second Phase Conclusion sentence
	public static String phase2Final(int num) {
		String conclusion = ("That " + subject(num) + " " + verb(num) + " her " + object(num) + "!");
		return conclusion;
	}
	
	// Method for a list of adjectives
	public static String adj(int num) {
		switch ( num ) {
		case 0:
			return "sleepy";
		case 1:
			return "big";
		case 2:
			return "little";
		case 3:
			return "beautiful";
		case 4:
			return "quick";
		case 5:
			return "pink";
		case 6:
			return "lazy";
		case 7:
			return "smart";
		case 8:
			return "happy";
		case 9:
			return "sad";
			default:
				return " ";
		}
	}
	
	// Method for a list of verbs
	public static String verb(int num) {
		switch ( num ) {
		case 0:
			return "jumped";
		case 1:
			return "climbed";
		case 2:
			return "ate";
		case 3:
			return "pushed";
		case 4:
			return "hit";
		case 5:
			return "used";
		case 6:
			return "laughed at";
		case 7:
			return "broke";
		case 8:
			return "borrowed";
		case 9:
			return "moved";
			default:
				return "";

		}
	}
	
	// Method for a list of subjects
	public static String subject(int num) {
		switch ( num ) {
		case 0:
			return "rabbit";
		case 1:
			return "girl";
		case 2:
			return "company manager";
		case 3:
			return "baby";
		case 4:
			return "doll";
		case 5:
			return "dog";
		case 6:
			return "woman";
		case 7:
			return "man";
		case 8:
			return "kid";
		case 9:
			return "fox";
			default:
				return " ";
		}
	}
	
	// Method for a list of objects
	public static String object(int num) {
		switch ( num ) {
		case 0:
			return "car";
		case 1:
			return "cake";
		case 2:
			return "homework";
		case 3:
			return "ball";
		case 4:
			return "bed";
		case 5:
			return "book";
		case 6:
			return "phone";
		case 7:
			return "computer";
		case 8:
			return "pen";
		case 9:
			return "apple";
			default:
				return " ";
		}
	}

} // end of class
