// Name: Yifan Zhang  Course information: CSE2 HW6  Date: 2018-10-21
// This program aims to develop a secret message X hiding in lots of patterned stars.

import java.util.Scanner; // import a scanner
// Start of class
public class EncryptedX {
	// Start of main method
	public static void main(String[] args) {
		// Create a Scanner
		Scanner input = new Scanner(System.in);
		int size = 0;
		// Prompt for input of size of square
		System.out.print("Enter an integer between 0 and 100 as the size of square: ");
	  
    
		boolean condition = false; // set the initial condition to false
		while( condition == false ) {
		   if( input.hasNextInt() == true ) {
			   size = input.nextInt(); // read the input and store its value if it is an integer
			   if( size <= 0 || size > 100 ) {
				   condition = false; // let the while loop run again if integer is out of range
				   System.out.print("Need to input an integer between 0 and 100: ");
				   }
			   else {
				   condition = true; // get out of the inner statement and while loop if condition is true
				   }    
		       } // end of the inner if statement
		   
		   else {
			   input.next(); // get rid of the value stored in the input scanner
			   System.out.print("Need to input an integer between 0 and 100: ");
			   } // end of the outer if statement
		   
		   }// end of while loop
    
		// Print the rows
		for ( int i = 0; i < size; i++ ) {
			// Print the columns
			for ( int j = 0; j < size; j++ ) {
				// Print a space for two diagonals
				if ( i == j || j == size -1 - i ) {
					System.out.print(" ");
				}
				// Print a space for others that are not a space
				else {
					System.out.print("*");
				} // end of inner for loop
			}
			System.out.println(); // Start a new line after each line
		} // end of outer for loop
		
	} // end of main method

} // end of class