// Name: Yifan Zhang. Course information: CSE2 Lab10  Date: 2018-12-6
// This program demonstrates selectionsort.

import java.util.Arrays;

public class SelectionSortLab10 {
	public static void main(String[] args) {
		int[] myArrayBest = {1,2,3,4,5,6,7,8,9};
		int[] myArrayWorst = {9,8,7,6,5,4,3,2,1};
		int iterBest = selectionSort(myArrayBest);
		int iterWorst = selectionSort(myArrayWorst);
		System.out.println("The total number of operations performed on the sorted array: " + iterBest);
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
	}
	
	public static int selectionSort(int[] list) {
		// Print the initial array
		for (int i = 0; i < list.length; i++) {
			System.out.print(list[i] + " ");
		}
		
		System.out.println(Arrays.toString(list));
		
		int iterations = 0;
		
		for (int i = 0; i < list.length - 1; i++) {
			iterations++;
			
			// Find the minimum in the list
			int currentMin = list[i];
			int currentMinIndex = i;
			for (int j = i + 1; j < list.length; j++) {
				if (list[j] > currentMin) {
					currentMin = list[j];
					currentMinIndex = j;
					
				}
				iterations++;
			} // for loop
			
			if (currentMinIndex != i) {
				int temp = list[i];
				list[i] = currentMin;
				list[currentMinIndex] = temp;
			}
		}
		return iterations;
		
		
		
	} // end of selectionsort method


}
